USE asterixdb;

-- Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
SELECT '1';
SELECT * FROM potion;

-- Liste des noms des trophées rapportant 3 points. (2 lignes)
SELECT '2';
SELECT NomCateg FROM categorie 
WHERE NbPoints = 3;

-- Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)
SELECT '3';
SELECT NomVillage FROM village 
WHERE NbHuttes > 35;

-- Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)
SELECT '4';
SELECT NumTrophee FROM trophee 
WHERE DatePrise between '2052-05-01' and '2052-06-30';

-- Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT '5';
SELECT Nom FROM habitant 
WHERE Nom like "a%r%";

-- Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT '6';
SELECT DISTINCT NumHab FROM absorber 
WHERE NumPotion = 1
  OR NumPotion = 3
  OR NumPotion = 4;

-- Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)
SELECT '7';
SELECT NumTrophee, DatePrise, categorie.NomCateg, habitant.Nom FROM trophee
LEFT JOIN categorie 
  ON trophee.CodeCat = categorie.CodeCat
LEFT JOIN habitant 
  ON NumPreneur = habitant.NumHab;

-- Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT '8';
SELECT Nom FROM habitant
LEFT JOIN village
  ON habitant.NumVillage = village.NumVillage
WHERE NomVillage = 'Aquilona';

-- Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)
SELECT '9';
SELECT Nom FROM trophee
LEFT JOIN habitant ON trophee.NumPreneur = habitant.NumHab
LEFT JOIN categorie ON trophee.CodeCat = categorie.Codecat
WHERE NomCateg = 'Bouclier de Légat';

-- Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes) 
SELECT '10';
SELECT LibPotion, Formule, ConstituantPrincipal FROM fabriquer
LEFT JOIN potion ON fabriquer.NumPotion = potion.NumPotion
LEFT JOIN habitant ON fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = 'Panoramix';

-- Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)
SELECT '11';
SELECT DISTINCT LibPotion FROM absorber
LEFT JOIN potion ON absorber.NumPotion = potion.NumPotion 
LEFT JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE habitant.Nom = 'Homéopatix';

-- Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT '12';
SELECT DISTINCT habitant.Nom FROM absorber
LEFT JOIN potion ON absorber.NumPotion = potion.NumPotion
LEFT JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
LEFT JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE fabriquer.NumHab = 3;

-- Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT '13';
SELECT DISTINCT habitant.Nom FROM absorber
LEFT JOIN potion ON absorber.NumPotion = potion.NumPotion
LEFT JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
LEFT JOIN habitant ON absorber.NumHab = habitant.NumHab 
WHERE fabriquer.NumHab = (SELECT NumHab FROM habitant WHERE Nom = 'Amnésix');

-- Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)
SELECT '14';
SELECT Nom FROM habitant WHERE NumQualite is NULL;

-- Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes)
SELECT '15';
SELECT habitant.Nom FROM absorber
LEFT JOIN habitant 
  ON absorber.NumHab = habitant.NumHab
LEFT JOIN potion 
  ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = 'potion magique n°1'
 AND absorber.DateA between '2052-02-01' and '2052-02-29';

-- Nom et âge des habitants par ordre alphabétique.

SELECT '16';
SELECT Nom, age FROM habitant ORDER BY Nom ASC;

-- Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)

SELECT '17';
SELECT resserre.NomResserre, village.NomVillage FROM resserre
LEFT JOIN village 
  ON resserre.NumVillage = village.NumVillage
ORDER BY resserre.superficie DESC;

-- Nombre d'habitants du village numéro 5. (4)
SELECT '18';
SELECT COUNT(*) FROM habitant WHERE NumVillage = 5;

-- Nombre de points gagnés par Goudurix. (5)
SELECT '19';
SELECT SUM(categorie.NbPoints) AS nb_points FROM trophee
LEFT JOIN categorie ON trophee.CodeCat = categorie.CodeCat
LEFT JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = 'Goudurix';

-- Date de première prise de trophée. (03/04/52)
SELECT '20';
SELECT DatePrise FROM trophee ORDER BY DatePrise ASC LIMIT 1;

-- Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)
SELECT '21';
SELECT SUM(absorber.Quantite) AS nombre_de_louches 
FROM absorber
LEFT JOIN potion 
  ON absorber.NumPotion = potion.NumPotion
WHERE potion.LibPotion = 'potion magique n°2';

-- Superficie la plus grande. (895)
SELECT '22';
SELECT superficie FROM resserre ORDER BY superficie DESC LIMIT 1;

-- Nombre d'habitants par village (nom du village, nombre). (7 lignes)
SELECT '23';
SELECT village.NomVillage, COUNT(*) AS nb_habitant FROM village
LEFT JOIN habitant ON village.NumVillage = habitant.NumVillage
GROUP BY village.NomVillage;

-- Nombre de trophées par habitant (6 lignes)
SELECT '24';
SELECT habitant.Nom, COUNT(*) AS nb_trophe FROM trophee
LEFT JOIN habitant ON trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom;

-- Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)
SELECT '25';
SELECT province.NomProvince, AVG(age) AS moyenne_age FROM province
LEFT JOIN village ON province.NumProvince = village.NumProvince
LEFT JOIN habitant ON habitant.NumVillage = village.NumVillage
GROUP BY province.NomProvince;

-- Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT '26';
SELECT habitant.Nom, COUNT(DISTINCT absorber.NumPotion) AS nb_potion_distinct_consome FROM absorber
LEFT JOIN habitant 
  ON habitant.NumHab = absorber.NumHab
GROUP BY habitant.Nom;

-- Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)
SELECT '27';
SELECT habitant.Nom FROM absorber
LEFT JOIN habitant 
  ON habitant.NumHab = absorber.NumHab
LEFT JOIN potion
  ON potion.NumPotion = absorber.NumPotion
WHERE potion.LibPotion = 'Potion Zen'
  AND absorber.Quantite > 2;

-- Noms des villages dans lesquels on trouve une resserre (3 lignes)
SELECT '28';
SELECT village.NomVillage FROM resserre
LEFT JOIN village 
  ON resserre.NumVillage = village.NumVillage;

-- Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT '29';
SELECT village.NomVillage FROM village
ORDER BY NbHuttes DESC
LIMIT 1;

-- Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes)
SELECT '30';
SELECT habitant.Nom, COUNT(*) FROM trophee
LEFT JOIN habitant ON habitant.NumHab = trophee.NumPreneur
GROUP BY trophee.NumPreneur
HAVING COUNT(*) > ( 
SELECT COUNT(*) FROM trophee 
LEFT JOIN habitant ON habitant.NumHab = trophee.NumPreneur 
WHERE habitant.Nom = 'Obélix'
GROUP BY trophee.NumPreneur );
